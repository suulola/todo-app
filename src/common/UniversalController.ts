import { IResponse } from "./interface"
import { Request } from 'express';

import { getLogger } from '@/utils/loggers';
const logger = getLogger('USER_ROUTE');

export class UniversalController {

  protected failureResponse = (message: string, data?: any): IResponse => {
    return ({ statusCode: 400, status: false, message: message || "failed", data: data || null })
  }

  protected successResponse = (message?: string, data?: any): IResponse => {
    return ({ statusCode: 200, status: true, message: message || "Success", data: data || null })
  }

  protected serviceErrorHandler = (req: Request, error: any) => {
    const { originalUrl, method, ip } = req;
    logger.error(`URL:${originalUrl} - METHOD:${method} - IP:${ip} - ERROR:${error}`)
    return ({ statusCode: 500, status: false, message: "Internal server error", data: null })
  }

}

