import mongoose from 'mongoose';

const MONGODB_URL: string = process.env.DATABASE_URL || '';

export const connectMongo = () => {
  mongoose.connect(MONGODB_URL);
};

const { connection } = mongoose;
connection.on('error', (error: any) => {
  console.log(`MongoDB database connection error: ${error}`);
  throw error;
});

connection.once('open', () => {
  console.log('MongoDB database connection opened successfully.');
});
