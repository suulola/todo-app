import { NextFunction, Request, Response } from "express";
import { Validator } from "node-input-validator";


export const inputValidator = (schema: any, type?: 'body' | 'query') => {

  return async (req: Request, res: Response, next: NextFunction) => {
    const data = type === 'query' ? req.query : req.body;
    console.log({ data })
    const v = new Validator(
      data,
      schema,
    );
    const matched = await v.check();
    console.log({ matched })
    if (matched) {
      return next();
    }
    return res.status(400).json({
      status: false,
      data: v.errors,
      message: 'invalid payload',
    });
  };
};