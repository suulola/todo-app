import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
	'action': String,
	'is_done': Boolean,
}, { timestamps: true });

export = mongoose.model('Todo', TodoSchema);
