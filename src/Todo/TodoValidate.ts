export const todoSchema = {
  action: 'required|minLength:5',
}
export const updateTodoSchema = {
  action: 'required:string',
  is_done: 'required:boolean',
}