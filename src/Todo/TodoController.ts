import TodoModel from './TodoModel';
import { Request, Response, NextFunction } from 'express';
import { TodoType } from './interface';
import { UniversalController } from '@/common/UniversalController';

/**
 * TodoController.ts
 *
 * @description :: Server-side logic for managing Todos.
 */

class TodoController extends UniversalController {

    /** TodoController.list() */
    public list = async (req: Request, res: Response, _next: NextFunction) => {
        try {
            const todos = await TodoModel.find();
            if (!todos) {
                return res.json(this.failureResponse(`'Error when getting Todo`))
            }
            return res.json(this.successResponse(`Todos Fetched successfully`, todos))
        } catch (error) {
            return res.json(this.serviceErrorHandler(req, error));
        }
    }

    /** TodoController.show() */
    public show = async (req: Request, res: Response, _next: NextFunction) => {
        try {
            const id = req.params.id;
            console.log({ id })
            const todo = await TodoModel.findOne({ _id: id });
            if (!todo) {
                return res.json(this.failureResponse(`'Error when getting Todo`, todo))
            }
            return res.json(this.successResponse(`Fetched successfully`, todo))
        } catch (error) {
            return res.json(this.serviceErrorHandler(req, error));
        }
    };

    /** TodoController.remove() */
    public remove = async (req: Request, res: Response, _next: NextFunction) => {
        try {
            const id = req.params.id;
            const todo = await TodoModel.findByIdAndRemove(id);
            if (todo) return res.json(this.successResponse(`Todo deleted`))
            return res.json(this.failureResponse(`Todo item doesn't exist`))
        } catch (error) {
            return res.json(this.serviceErrorHandler(req, error))
        }
    };

    /** TodoController.update() */
    public update = async (req: Request, res: Response, _next: NextFunction) => {
        try {
            const { action, is_done } = req.body
            const id = req.params.id;
            const todo = await TodoModel.findOne({ _id: id });
            if (todo) {
                todo.action = action ? action : todo.action;
                todo.is_done = is_done ? is_done : todo.is_done;
                const updatedTodo = await todo.save();
                if (updatedTodo) {
                    return res.json(this.successResponse(`Todo update successful`, todo))
                }
            }
            return res.json(this.failureResponse(`Failed to update todo`))
        } catch (error) {
            return res.json(this.serviceErrorHandler(req, error));
        }
    }

    public create = async (req: Request, res: Response, _next: NextFunction) => {
        try {
            const model: TodoType = {
                action: req.body.action,
                is_done: false,
            }
            const todo = await new TodoModel(model).save();
            if (todo) return res.json(this.successResponse(`Todo created successfully`, todo))
            return res.json(this.failureResponse(`Todo removal failed`))
        } catch (error) {
            return res.json(this.serviceErrorHandler(req, error));

        }
    }
}

export default new TodoController()