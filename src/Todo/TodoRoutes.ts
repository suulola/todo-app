import * as express from 'express';
import TodoController from './TodoController';
import { inputValidator } from '@/utils/middleware';
import { todoSchema, updateTodoSchema } from './TodoValidate';


const todoRouter = express.Router();

/*
 * GET
 */
todoRouter.get('/', TodoController.list);

// @desc        Route to phoneName
// @route       GET /api/v1/accounts/nipFees
// @access      Public
todoRouter.get('/:id', TodoController.show);

/*
 * POST
 */
todoRouter.post('/', inputValidator(todoSchema), TodoController.create);

/*
 * PUT
 */
todoRouter.put('/:id', inputValidator(updateTodoSchema), TodoController.update);

/*
 * DELETE
 */
todoRouter.delete('/:id', TodoController.remove);

export default todoRouter;
