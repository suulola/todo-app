export interface TodoType {
  action: string,
  is_done: boolean,
  created_date?: number
}