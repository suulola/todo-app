import axios from 'axios'
import React, { useEffect, useState } from 'react'
import './App.css'

interface ResponseType {
  status: boolean
  statusCode: number
  data?: any
  message?: string
}

interface Todo {
  action: string
  updatedAt?: string
  createdAt: string
  is_done: boolean
  _id: string
}

const TODO_API = `/api/v1/todo`
const defaultForm = { action: '', id: '', is_done: false }

const App = () => {
  const [allTodos, setAllTodos] = useState([])
  const [form, setForm] = useState(defaultForm)

  const handleFetchData = async () => {
    try {
      const request: ResponseType = await (await axios(TODO_API)).data
      if (request.status) {
        setAllTodos(request.data)
      }
    } catch (error) {
      console.log({ error })
    }
  }

  const handleAddOrUpdate = async (): Promise<ResponseType> => {
    const model = { action: form.action, is_done: form.is_done }
    if (form.id) {
      return await (await axios.put(`${TODO_API}/${form.id}`, model)).data
    }
    return await (await axios.post(TODO_API, model)).data
  }

  const handleAddTodo = async () => {
    try {
      if (!form.action) return
      await handleAddOrUpdate()
      setForm(defaultForm)
      handleFetchData()
    } catch (error) {
      console.log({ error })
    }
  }

  const handleDelete = async (id: string) => {
    try {
      await axios.delete(`${TODO_API}/${id}`)
      handleFetchData()
    } catch (error) {
      console.log({ error })
    }
  }

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm(prev => ({ ...prev, action: e.target.value }))
  }

  useEffect(() => {
    handleFetchData()
  }, [])

  return (
    <section>
      <main>
        <header className='row'>
          <input value={form.action} onChange={handleInput} />
          <button onClick={handleAddTodo}>{form.id ? 'Update' : 'Add'}</button>
        </header>
        {allTodos.map(
          ({ action, is_done, createdAt, _id, updatedAt }: Todo) => (
            <div key={_id} className='row spacing'>
              <div className='details'>
                <h5>{action}</h5>
                <small>{(updatedAt ?? createdAt).slice(0, 10)}</small>
              </div>
              <div>
                <button onClick={() => setForm({ action, is_done, id: _id })}>
                  Edit
                </button>
                <button onClick={() => handleDelete(_id)}>Delete</button>
              </div>
            </div>
          )
        )}
      </main>
    </section>
  )
}

export default App
